import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import {texts} from "../data";

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

router
    .get("/texts/:id", (req, res) => {
        const id = parseInt(req.params.id);
        if(isNaN(id)) res.status(400).send("\"id\" must be an index");
        else if(id < 0 || id >= texts.length) res.status(422).send("\"id\" not in range");
        else res.status(200).send(texts[id]);
    });

export default router;
