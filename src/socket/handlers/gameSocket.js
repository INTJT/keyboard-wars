import {namespaces} from "../namespaces";
import {rooms} from "./roomsSocket";
import {users} from "./usersSocket";
import {errors} from "../errors";
import {Game, Room, roomStatutes} from "../entities";
import {gameEvents} from "../events/gameEvents";
import {SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "../config";
import {texts} from "../../data";

export function gameSocket(io, { createRoom, killRoom, updateVisibility }) {

    const gameNamespace = io.of(namespaces.game);

    gameNamespace.use((socket, next) => {

        const {username, roomName, create} = socket.handshake.query;

        //must have username param
        if(!username) {
            next(new Error(errors.NO_USERNAME));
            return;
        }

        const user = users.find(user => user.name === username);

        //user must be in users
        if(!user) {
            next(new Error(errors.USER_NOT_FOUND));
            return;
        }

        //user shouldn't already be in the room
        if(user.ready !== undefined) {
            next(new Error(errors.ALREADY_IN_ROOM));
            return;
        }

        //must have roomName param
        if(typeof roomName !== "string" || roomName === "") {
            next(new Error(errors.NO_ROOM));
            return;
        }

        const room = rooms.find(room => room.name === roomName);

        //room shouldn't exists if create is true and vise versa
        if(Boolean(room) === Boolean(create)) {
            next(new Error(create ? errors.ROOM_ALREADY_EXISTS : errors.NO_ROOM));
            return;
        }

        if(!create) {

            //user can't join the room if game is started
            if(room.status !== roomStatutes.WAITING) {
                next(new Error(errors.ALREADY_IN_ROOM));
                return;
            }

            //user can't join the room if room is full
            if(room.isFull) {
                next(new Error(errors.TOO_MANY_USERS));
                return;
            }

        }

        next();

    });

    gameNamespace.on("connection", socket => {

        const { username, roomName, create } = socket.handshake.query;

        const user = users.find(user => user.name === username);
        const room = create ? new Room(roomName) : rooms.find(room => room.name === roomName);
        room.join(user);

        if(create) createRoom(room);
        else updateVisibility(room);

        socket.join(roomName);
        socket.emit(gameEvents.SET_UP, room);
        gameNamespace.to(roomName).emit(gameEvents.JOIN, user);

        function startTimer() {

            const index = Math.floor(Math.random() * texts.length);
            room.game = new Game(room, texts[index]);
            room.status = roomStatutes.TIMER;

            updateVisibility(room);

            const timer = new Date();
            timer.setSeconds(timer.getSeconds() + SECONDS_TIMER_BEFORE_START_GAME);

            gameNamespace.to(roomName).emit(gameEvents.START_TIMER, { timer, index });

            setTimeout(() => {
                if(!room.isEmpty) startGame()
            }, SECONDS_TIMER_BEFORE_START_GAME * 1000);

        }

        function startGame() {

            room.status = roomStatutes.GAME;

            const started = room.game;

            const timer = new Date();
            timer.setSeconds(timer.getSeconds() + SECONDS_FOR_GAME);
            gameNamespace.to(roomName).emit(gameEvents.START_GAME, timer);

            setTimeout(() => {
                if(!room.isEmpty && room.game === started) endGame();
            }, SECONDS_FOR_GAME * 1000);

        }

        function endGame() {

            const results = room.game.results;
            delete room.game;
            room.status = roomStatutes.WAITING;
            room.users.forEach(user => user.ready = undefined);

            gameNamespace.to(roomName).emit(gameEvents.SHOW_RESULTS, results);
            gameNamespace.to(roomName).emit(gameEvents.SET_UP, room);

            updateVisibility(room);

        }

        socket.on(gameEvents.TOGGLE_READY, () => {
            if(room.status === roomStatutes.WAITING) {
                user.ready = !user.ready;
                gameNamespace.to(roomName).emit(gameEvents.UPDATE_USER, user);
                if(room.needStart) startTimer();
            }
        });

        socket.on(gameEvents.TYPE_CHAR, char => {
            if(room.status === roomStatutes.GAME) {
                const current = room.game.type(user, char);
                if(typeof current === "number") {
                    gameNamespace.to(roomName).emit(gameEvents.UPDATE_USER, { ...user, typing: current });
                    if(room.needEnd) endGame();
                }
            }
        });

        socket.on("disconnect", () => {
            room.leave(user);
            if(room.needEnd) endGame();
            if(!room.isEmpty) {
                gameNamespace.to(roomName).emit(gameEvents.LEAVE, user);
                if(room.needStart) startTimer();
                updateVisibility(room);
            }
            else killRoom(room);
        });

    });

}
