import {roomsEvents} from "../events/roomsEvents";
import {namespaces} from "../namespaces";

export const rooms = [];
const hidden = [];

export function roomsSocket(io) {

    const roomsNamespace = io.of(namespaces.rooms);

    function createRoom(room) {
        const roomsIndex = rooms.indexOf(rooms.find(r => r.name === room.name));
        if(roomsIndex === -1) {
            rooms.push(room);
            roomsNamespace.emit(roomsEvents.ADD_ROOM, room);
        }
        else console.error(`Trying add "${room.name}" room that already exists`);
    }

    function killRoom(room) {
        const roomsIndex = rooms.indexOf(rooms.find(r => r.name === room.name));
        const hiddenIndex = hidden.indexOf(hidden.find(r => r.name === room.name));
        if(roomsIndex !== -1 && rooms[roomsIndex] === room) rooms.splice(roomsIndex, 1);
        if(hiddenIndex !== -1 && hidden[hiddenIndex] === room) hidden.splice(hiddenIndex, 1);
        roomsNamespace.emit(roomsEvents.REMOVE_ROOM, room);
    }

    function updateVisibility(room) {
        const roomsIndex = rooms.indexOf(rooms.find(r => r.name === room.name));
        if(roomsIndex === -1 || rooms[roomsIndex] !== room) console.error(`Trying change visibility of "${room.name}" room that actually was deleted`);
        else {
            const hiddenIndex = hidden.indexOf(hidden.find(r => r.name === room.name));
            const needShow = room.needShow;

            //need hide but room's visible -> remove
            //need show but room's hidden -> add
            //need show and room's already visible -> update
            //need hide and room's already hidden -> nothing

            if (!needShow && hiddenIndex === -1) {
                hidden.push(room);
                roomsNamespace.emit(roomsEvents.REMOVE_ROOM, room);
            }
            if (needShow && hiddenIndex !== -1) {
                hidden.splice(hiddenIndex, 1);
                roomsNamespace.emit(roomsEvents.ADD_ROOM, room);
            } else if (needShow && hiddenIndex === -1) roomsNamespace.emit(roomsEvents.UPDATE_ROOM, room);
        }
    }

    roomsNamespace.on("connection", socket => {
        socket.emit(roomsEvents.UPDATE_ROOMS, rooms.filter(room => room.needShow));
    });

    return { createRoom, killRoom, updateVisibility };

}
