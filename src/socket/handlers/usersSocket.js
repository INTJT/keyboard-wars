import {User} from "../entities";
import {namespaces} from "../namespaces";
import {errors} from "../errors";

export const users = [];

export function usersSocket(io) {

    const usersNamespace = io.of(namespaces.users);

    usersNamespace.use((socket, next) => {
        const username = socket.handshake.query.username;
        if(username === undefined) next(new Error(errors.NO_USERNAME));
        if (users.find(user => user.name === username)) next(new Error(errors.USER_ALREADY_EXISTS));
        else next();
    });

    usersNamespace.on("connection", socket => {

        const user = new User(socket.handshake.query.username);
        users.push(user);

        socket.on("disconnect", () => {
            users.splice(users.indexOf(user), 1);
        });

    });

}