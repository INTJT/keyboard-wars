import {MAXIMUM_USERS_FOR_ONE_ROOM} from "./config";

export class User {

    constructor(name) {
        this.name = name;
        this.ready = undefined;
    }

}

export const roomStatutes = {
    WAITING: "WAITING",
    TIMER: "TIMER",
    GAME: "GAME"
}

export class Room {

    constructor(name) {
        this.name = name;
        this.users = [];
        this.status = roomStatutes.WAITING;
        this.game = undefined;
    }

    join(user) {
        user.ready = false;
        this.users.push(user);
    }

    leave(user) {
        user.ready = undefined;
        this.users.splice(this.users.indexOf(user), 1);
        if(this.status !== roomStatutes.WAITING) this.game.leave(user);
    }

    get allReady() { return this.users.every(user => user.ready); }

    get isEmpty() { return this.users.length === 0; }

    get isFull() { return this.users.length === MAXIMUM_USERS_FOR_ONE_ROOM; }

    get needShow() { return !this.isFull && this.status === roomStatutes.WAITING; }
    get needEnd() { return this.status !== roomStatutes.WAITING && this.game?.allComplete; }
    get needStart() { return !this.isEmpty && this.allReady && this.status === roomStatutes.WAITING; }

}

export class Game {

    constructor(room, text) {
        this.text = text;
        this.typing = new Map(room.users.map(user => [user.name, 0]));
        this.places = [];
    }

    type(user, char) {
        if(!this.typing.has(user.name)) return false;
        let typing = this.typing.get(user.name);
        let current = this.text[typing];
        if (current === char) {
            this.typing.set(user.name, ++typing);
            if (typing === this.text.length) {
                this.typing.delete(user.name);
                this.places.push(user.name);
            }
            return typing;
        }
        else return false;
    }

    leave(user) {
        this.typing.delete(user.name);
        const index = this.places.indexOf(user.name);
        if(index !== -1) this.places.splice(index, 1);
    }

    get allComplete() { return this.typing.size === 0; }

    get results() {
        const remaining = Array.from(this.typing)
            .sort((a, b) => b[1] - a[1])
            .map(pair => pair[0]);
        this.typing.clear();
        this.places.push(...remaining);
        return this.places;
    }

}
