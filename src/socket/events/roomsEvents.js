export const roomsEvents = {
    UPDATE_ROOMS: "UPDATE_ROOMS",
    ADD_ROOM: "ADD_ROOM",
    UPDATE_ROOM: "UPDATE_ROOM",
    REMOVE_ROOM: "REMOVE_ROOM"
}
