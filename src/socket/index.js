import {usersSocket} from "./handlers/usersSocket";
import {roomsSocket} from "./handlers/roomsSocket";
import {gameSocket} from "./handlers/gameSocket";

export default io => {
  usersSocket(io);
  const roomsFunctions = roomsSocket(io);
  gameSocket(io, roomsFunctions);
};
