import path from "path";

export const ENVIRONMENT = process.env.NODE_ENV || "dev";
export const STATIC_PATH = path.join(__dirname, "..", "public");
export const HTML_FILES_PATH = path.join(STATIC_PATH, "html");

export const PORT = Number(process.env.PORT) || 3002;
