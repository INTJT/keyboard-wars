import {ObservableMap} from "../observable.js";

export class ModalsService {

    static #modals = new ObservableMap();

    static get modals() { return ModalsService.#modals; }

    static pushModal(modal, callback) {
        modal.addEventListener("click", e => {
            e.stopPropagation();
        });
        ModalsService.modals.set(modal, callback);
    }

    static popModal(result) {
        const last = ModalsService.modals.array().pop();
        if(last) {
            const callback = ModalsService.modals.delete(last[0]);
            callback(result);
        }
    }

}
