FROM node:16-alpine

WORKDIR /usr/keyboard-wars

# Bundle application source
COPY . .

# Install  dependencies
RUN npm install

# Compile babel
RUN npm run build

# Remove devDependencies and setup for production
RUN npm prune --omit=dev

# Environment variables
ENV NODE_ENV production
ENV PORT 8008

# Deploy
EXPOSE $PORT
CMD PORT=$PORT NODE_ENV=$NODE_ENV npm run prod
